from random import randint
import datetime

class User():

    def __init__ (self,name="", points=0, errors=0, time=0):
        self.name=name
        self.points=points
        self.errors=errors
        self.time=time
    def setUsername(self):
        self.name=input("Enter Username: ")
    def startGame(self):
        beginGame = input("Type 'start' to start: ")

        # Will proceed until the user entered 'start'
        while not beginGame == 'start':
            beginGame = input("Type start to start: ")
        self.randProb()

    def randProb(self):
        self.points=0
        self.errors=0
        start = datetime.datetime.now()
        while self.points < 10 and self.errors < 10:
            prob = randint(1, 4)
            if (prob == 1):
                frstNum = randint(10, 99)
                sndNum = randint(10, 99)
                ans = check("Solve %d + %d: " % (frstNum, sndNum))
                if (ans == (frstNum + sndNum)):
                    print("Correct!")
                    self.points += 1
                else:
                    print("Wrong")
                    self.errors += 1
            elif (prob == 2):
                frstNum = randint(10, 99)
                sndNum = randint(10, 99)
                while (frstNum < sndNum):
                    frstNum = randint(10, 99)
                    sndNum = randint(10, 99)
                ans = check("Solve %d - %d: " % (frstNum, sndNum))
                if (ans == (frstNum - sndNum)):
                    print("Correct!")
                    self.points += 1
                else:
                    print("Wrong")
                    self.errors += 1
            elif (prob == 3):
                frstNum = randint(10, 99)
                sndNum = randint(2, 9)
                ans = check("Solve %d * %d: " % (frstNum, sndNum))
                if (ans == (frstNum * sndNum)):
                    print("Correct!")
                    self.points += 1
                else:
                    print("Wrong")
                    self.errors += 1
            elif (prob == 4):
                frstNum = randint(100, 999)
                sndNum = randint(2, 9)
                while (frstNum % sndNum != 0):
                    frstNum = randint(10, 99)
                    sndNum = randint(10, 99)
                ans = check("Solve %d / %d: " % (frstNum, sndNum))
                if (ans == (frstNum / sndNum)):
                    print("Correct!")
                    self.points += 1
                else:
                    print("Wrong")
                    self.errors += 1
            else:
                print("invalid")

        end = datetime.datetime.now()
        elapsed = end - start
        self.time=str(elapsed.seconds // 60 % 60) + ":" + str((elapsed.seconds % 60))
        if (self.points == 10):
            print("Congratulations! you pass")
        elif (self.errors == 10):
            print("Sorry, you failed")

        print("Score: points: %d errors: %d time: %s" % (self.points, self.errors, self.time))


#value check
def check(string):
    while True:
        try:
            answer=int(input(string))
            return answer
        except ValueError:
            print("Invalid value, please enter again \n ")
            continue

#load data to list
def loadData():
    lst=[]
    f=open('data.txt', 'r')
    for record in f.readlines():
        temp=record.split(", ")
        usr=User(temp[0],temp[1],temp[2],temp[3])
        lst.append(usr)
    return lst

#add data to file
def addData(user):
    f=open('data.txt','a')
    f.write(str(user.name)+", "+str(user.points)+", "+str(user.errors)+", "+str(user.time)+"\n")
    f.close()

#sort records
def sortLst(lstOfUsr):
    lstOfUsr.sort(key=lambda x:(x.errors,x.time))
    return lstOfUsr

#check duplicate; if yes and if time < old time , replace
def checkIfExist(user, lstOfUsr):
    count=0
    condition= False
    for usr in lstOfUsr:
        if (user.name in usr.name):
                if(user.time<usr.time):
                    lstOfUsr[count]=user
                    break
        count += 1

    return lstOfUsr, condition

#display users in sorted order
def display(lstOfUser):
    for user in lstOfUser:
        print(str(user.name)+ ", " + str(user.points)+ ", " + str(user.errors)+ ", " + str(user.time))
def main():
    lstOfUsr=loadData()
    #Will only exit if the user press enter
    display(lstOfUsr)
    while True:
        user=User()
        user.setUsername()
        condition=False
        if(user.name==""):
            #sort object
            lstOfUsr=sortLst(lstOfUsr)
            display(lstOfUsr)
            break
        user.startGame()
        if(user.errors<10):
           if(len(lstOfUsr)!=0):
               #check if exist
               lstOfUsr, condition =checkIfExist(user,lstOfUsr)
           if(not condition):
               addData(user)
               lstOfUsr.append(user)

if __name__ == '__main__':
    main()

